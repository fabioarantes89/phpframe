<?php
class RepresentantesBlock extends DefaultBlock
{
	public function estadoBox($params = null)
	{
		$criteria = new FuriousSelectCriteria();
		$criteria->addAscendingOrder("nome");
		$this->estados = EstadoModel::doSelect($criteria);
		$this->estadoEscolhido = $params['CONTENT']->CNT_CKY;

	}


}
?>