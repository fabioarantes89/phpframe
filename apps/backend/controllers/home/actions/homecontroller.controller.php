<?php
	class HomeController extends DefaultBackEnd2Controller
	{
		public function index($vars = null)
		{
			$ID = Dispatcher::getEditorialID();
			$user = Dispatcher::getUserSession();

			$editorial = ProdutoModel::getOne($ID);
			$editorial = $editorial[0];

			$this->siteSel = $editorial->getSite();


			// Editorial Itens
			$criteria = new FuriousSelectCriteria();
			$criteria->addJoin(FuriousExpressionsDB::LEFT_JOIN, "`w11_produto_menu_grupo`", "`w11_produto_menu_grupo`.`MSC_IMN`", "`w11_produto_menu`.`MNU_ID`");
			$criteria->add("`w11_produto_menu_grupo`.`MSC_IGR`", $user->getUSUGRP(), FuriousExpressionsDB::EQUAL);
			$criteria->add("`w11_produto_menu`.`MNU_IPR`", $ID, FuriousExpressionsDB::EQUAL);
			$criteria->add("`w11_produto_menu`.`MNU_STS`", 1, FuriousExpressionsDB::EQUAL);
			$criteria->addGroupBy("`w11_produto_menu`.`MNU_ID`");
			$menus = ProdutomenuModel::doSelect($criteria);

			$menusArr = array();

		

			if($ID != $this->siteSel->getPDTID())
			{

				// Site Itens
				$criteria = new FuriousSelectCriteria();
				$criteria->addJoin(FuriousExpressionsDB::LEFT_JOIN, "`w11_produto_menu_grupo`", "`w11_produto_menu_grupo`.`MSC_IMN`", "`w11_produto_menu`.`MNU_ID`");
				$criteria->add("`w11_produto_menu_grupo`.`MSC_IGR`", $user->getUSUGRP(), FuriousExpressionsDB::EQUAL);
				$criteria->add("`w11_produto_menu`.`MNU_IPR`", $this->siteSel->getPDTID(), FuriousExpressionsDB::EQUAL);
				$criteria->add("`w11_produto_menu`.`MNU_STS`", 1, FuriousExpressionsDB::EQUAL);
				$criteria->add("`w11_produto_menu`.`MNU_TIP`", '^([a-z]{2}[0-9]{1}|[a-z]{2})/index', FuriousExpressionsDB::NOT_REGEXP);
				$criteria->addAscendingOrder("`w11_produto_menu`.`MNU_GRP`");
				$criteria->addAscendingOrder("`w11_produto_menu`.`MNU_ORD`");
				$criteria->addGroupBy("`w11_produto_menu`.`MNU_ID`");

				$menus2 = ProdutomenuModel::doSelect($criteria);
				$menusArr = $menus2;

			}
			foreach($menus as $menu)
			{
				$menusArr[] = $menu;
			}
			$menus = $menusArr;

			$this->conts = array();
			if(!empty($menus[0]))
			{


				foreach($menus as $item)
				{
					if($item != NULL)
					{
						$link = $item->getMNUTIP();
						preg_match("/^([a-z0-9_-]+)\//i", $link, $matches);
						if(strlen($matches[1]) == "2")
						{
							if(file_exists(APP_PATH_PREFIX."apps/".APP_NAME."/controllers/featured/actions/featuredblock.block.php"))
							{
								$this->conts["featured"][] = array("EditorialID" => $ID, "MODULE" => $matches[1], "MENU" => $item);
							}
						}
						if(file_exists(APP_PATH_PREFIX."apps/".APP_NAME."/controllers/".$matches[1]."/actions/".$matches[1]."block.block.php"))
						{
							$this->conts[$matches[1]][] = array("EditorialID" => $ID, "MODULE" => $matches[1], "MENU" => $item);
						}
					}

				}
			}

		}
		public function redirect($vars = null)
		{
			$user = Dispatcher::getUserSession();

			$criteria = new FuriousSelectCriteria();
			$criteria->addJoin(FuriousExpressionsDB::LEFT_JOIN, "`w11_produto_grupo`", "`w11_produto_grupo`.`SEC_IPR`", "`w11_produto`.`PDT_ID`");
			$criteria->add("`w11_produto_grupo`.`SEC_IGR`", ((int)$user->getUSUGRP()), FuriousExpressionsDB::EQUAL);
			$criteria->add("`w11_produto`.`PDT_PAI`", 0, FuriousExpressionsDB::EQUAL);
			$criteria->add("`w11_produto`.`PDT_STS`", 1, FuriousExpressionsDB::EQUAL);
			$criteria->setLimit(1);

			$itens = ProdutoModel::doSelect($criteria);
			if(!empty($itens[0]))
			{
				$item = $itens[0];
				Dispatcher::forwardRaw($item->getURL());
			}
		}
		public function getPdtMenus($vars = null)
		{
			$user = Dispatcher::getUserSession();
			$criteria = new FuriousSelectCriteria();
			$criteria->addJoin(FuriousExpressionsDB::LEFT_JOIN, "`w11_produto_menu_grupo`", "`w11_produto_menu_grupo`.`MSC_IMN`", "`w11_produto_menu`.`MNU_ID`");
			$criteria->add("`w11_produto_menu`.`MNU_IPR`", addslashes($_GET['IPR']), FuriousExpressionsDB::EQUAL);
			$criteria->add("`w11_produto_menu`.`MNU_STS`", 1, FuriousExpressionsDB::EQUAL);
			$criteria->add("`w11_produto_menu_grupo`.`MSC_IGR`", $user->getUSUGRP(), FuriousExpressionsDB::EQUAL);
			$criteria->addAscendingOrder("`w11_produto_menu`.`MNU_GRP`");

		}
		public function tags($vars = null)
		{
			if(!empty($_GET["addTag"]))
			{
				$criteria = new FuriousSelectCriteria();
				$criteria->add("`cnt_tags`.`TAG_SLU`", addslashes(Slugfy(utf8_decode($_GET['addTag']))), FuriousExpressionsDB::EQUAL);
				$criteria->add("`cnt_tags`.`TAG_STS`", 1, FuriousExpressionsDB::EQUAL);
				$res = TagsModel::doSelect($criteria);

				if(empty($res[0]))
				{
					$tag = new Tags();
					$tag->TAG_NOM = addslashes(utf8_decode($_GET["addTag"]));
					$tag->TAG_SLU = addslashes(Slugfy(utf8_decode($_GET["addTag"])));
					$tag->TAG_STS = 1;
					$tag->save();
					//$tag->generateJSON();
				}
			}

			$criteria = new FuriousSelectCriteria();
			$criteria->add("`cnt_tags`.`TAG_STS`", 1, FuriousExpressionsDB::EQUAL);
			$criteria->add("`cnt_tags`.`TAG_NOM`", addslashes(utf8_decode($_GET["term"])."%"), FuriousExpressionsDB::LIKE);
			$criteria->addAscendingOrder("`cnt_tags`.`TAG_NOM`");
			$this->tags = TagsModel::doSelect($criteria);

		}

		public function categoria($vars = null)
		{

			if(!empty($_GET["categoria"]))
			{

				$criteria = new FuriousSelectCriteria();
				$criteria->add("CAT_NOM", addslashes(Slugfy(utf8_decode($_GET['categoria']))), FuriousExpressionsDB::EQUAL);
				$criteria->add("`cnt_categorias`.`CAT_TIP`", "REC", FuriousExpressionsDB::EQUAL);
				$criteria->add("CAT_STS", 1, FuriousExpressionsDB::EQUAL);

				$res = CategoriasModel::doSelect($criteria);


				if(empty($res[0]))
				{
					$categoria = new Categorias();
					$categoria->CAT_NOM = addslashes(utf8_decode($_GET["categoria"]));
					$categoria->CAT_TIP = $this->Site->getPDTID();
					$categoria->CAT_COR = "REC";
					$categoria->CAT_STS = 1;
					$categoria->save();
					//$tag->generateJSON();
				}
			}

			$criteria = new FuriousSelectCriteria();
			$criteria->add("`cnt_categorias`.`CAT_STS`", 1, FuriousExpressionsDB::EQUAL);
			$criteria->add("`cnt_categorias`.`CAT_COR`", "REC", FuriousExpressionsDB::EQUAL);
			$criteria->add("`cnt_categorias`.`CAT_NOM`", addslashes(utf8_decode($_GET["term"])."%"), FuriousExpressionsDB::LIKE);
			$criteria->addAscendingOrder("CAT_NOM");
			$this->tags = CategoriasModel::doSelect($criteria);
			$this->setTemplate('tags');

		}
	}
?>
