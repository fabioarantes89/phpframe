<?php
class CidadesModel extends ActiveModel {
	protected static $ActiveObject = "Cidades";
	public static $TABLE = "cnt_cidades";
	protected static $fields = array('CDD_ID', 'CDD_UF', 'CDD_TIT');
	
	static function doCount($criteria)
	{
		self::getDatabase();
		$criteria->addFields(array("COUNT(*) as totalNews"));
		$criteria->setTable(self::$TABLE);
		$res = self::$database->executeQuery($criteria);
		$total = self::$database->fetch_object($res);
		return ((int)$total->totalNews);
	}
	static function doDelete(ActiveObject $obj, $class = null)
	{
		return parent::doDelete($obj, __CLASS__);
	}
	static function doUpdate(ActiveObject $obj, $class = null)
	{
		return parent::doUpdate($obj, __CLASS__);
	}
	static function doSave(ActiveObject $obj, $class = null)
	{
		return parent::doSave($obj, __CLASS__);
	}
	static function doPaginatedSelect($criteria, $class = null){
		return parent::doPaginatedSelect($criteria, __CLASS__);
	}
	static function getAll($activeModel = null) {
		return parent::getAll(__CLASS__);
	}
	static function getOne($ID, $activeModel = null) {
		return parent::getOne($ID, __CLASS__);
	}
	static function doSelect($criteria, $class = null){
		return parent::doSelect($criteria, __CLASS__);
	}
}
?>