<?php
	class Cidades extends ActiveObject {
		protected $activeModel = "CidadesModel";
		protected $FID = "CDD_ID";
		public function getCDDID()
		{
			return $this->returnKey("CDD_ID");
		}
		public function getCDDUF()
		{
			return $this->returnKey("CDD_UF");
		}
		public function getCDDTIT()
		{
			return $this->returnKey("CDD_TIT");
		}
		
		public function getProperties()
		{
			return $this->params;
		}
	}
?>