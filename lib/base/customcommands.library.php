<?php
if(!function_exists('get_called_class')) { 
 function get_called_class($debug = false) {
    $bt = debug_backtrace(); 
	if($bt[1]['function'] == "doSelect" && $bt[2]['function'] == "doPaginatedSelect")
	{
		foreach($bt[3]['args'] as $arg){
			preg_match("/([a-z0-9]+)::([a-z0-9_]+)/i", $arg, $matches);
			if($matches[2] == "doPaginatedSelect"){
				return $matches[1];
			}
		}
	} else if($bt[1]['function'] == "doSelect" && $bt[2]['function'] != "doPaginatedSelect"){
		return $bt[2]['object']->getActiveModel();
	}
  }
} 
if(!function_exists('wave_area'))
{
	function wave_area($img, $x, $y, $width, $height, $amplitude = 10, $period = 10){ 
	    // Make a copy of the image twice the size 
	    $height2 = $height * 2; 
	    $width2 = $width * 2; 
	    $img2 = imagecreatetruecolor($width2, $height2); 
	    imagecopyresampled($img2, $img, 0, 0, $x, $y, $width2, $height2, $width, $height);
	    if($period == 0) $period = 1; 
	    // Wave it 
	    for($i = 0; $i < ($width2); $i += 2){
			imagecopy($img2, $img2, $x + $i - 2, $y + sin($i / $period) * $amplitude, $x + $i, $y, 2, $height2); 
		}
	    // Resample it down again 
	    imagecopyresampled($img, $img2, $x, $y, 0, 0, ($width+2), $height, $width2, $height2); 
	    imagedestroy($img2); 
	}
}


if(!function_exists('Slugfy'))
{
	function Slugfy($string)
	{
		$chars = array(
			" " => "-",
			"_" => "-",
			"." => "-",
			"," => "",
			"/" => "",
			"�" => "a",
			"�" => "a",
			"�" => "a",
			"�" => "a",
			"�" => "a",
			"�" => "e",
			"�" => "e",
			"�" => "e",
			"�" => "e",
			"�" => "i",
			"�" => "i",
			"�" => "i",
			"�" => "i",
			"�" => "o",
			"�" => "o",
			"�" => "o",
			"�" => "o",
			"�" => "o",
			"�" => "u",
			"�" => "u",
			"�" => "u",
			"�" => "u",
			"�" => "c",
			"\"" => "",
			"\'" => "",
			"?" => ""
		);
		$newStr = str_replace(array_keys($chars), array_values($chars), strtolower($string));
		return $newStr;
	}
}
if(!function_exists('Slugfy2'))
{
	function Slugfy2($string)
	{
		return strtolower(preg_replace('/[^a-zA-Z0-9]/i', '-', $string));
	}
}
if(!function_exists("renderArrayOption"))
{
	function renderArrayOption($item = null, $TabIndex = 0, $writeParent = true)
	{
		$itens = array();
		$tabs = "";
		if($writeParent != true)
		{
			$TabIndex--;
		}
		for($i=0; $i<$TabIndex; $i++){
			$tabs .= "&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		if($writeParent == true)
		{
			$itens[$item->getId()] = $tabs . $item->getName();
		}
		if(method_exists($item, "hasChildren"))
		{
			if($item->hasChildren())
			{

				$itn = $item->getChildren();
				foreach($itn as $child){
					$itn2 = renderArrayOption($child, ($TabIndex+1));
					$itens = $itens + $itn2;
				}


			}
		}		
		return $itens;
	}
}
if(!function_exists("renderArraySubmenu"))
{
	function renderArraySubmenu($item = null, $TabIndex = 0)
	{
		$itens = array();
		$tabs = "";
		for($i=0; $i<$TabIndex; $i++){
			$tabs .= "&nbsp;&nbsp;&nbsp;&nbsp;";
		}
		
		$itens[$item->getId()] = $tabs . $item->getName();
		
		if($item->hasChildren())
		{
			
			$itn = $item->getChildren();
			foreach($itn as $child){
				$itn2 = renderArraySubmenu($child, ($TabIndex+1));
				$itens = $itens + $itn2;
			}
			
			
		}
		
		return $itens;
	}
}

?>